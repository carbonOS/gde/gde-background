# GDE Background

`gde-background` simply renders the wallpaper behind the carbonOS
desktop. It re-implements the logic from gnome-shell, so it should
be API-compatible w/ GNOME (i.e. reading from GNOME settings,
timed animations, etc)

### Building

Development is easiest on carbonOS through GNOME Builder. Simply switch carbonOS
to the devel variant:

```bash
$ updatectl switch --base=devel
[Enter your credentials at the prompt]
[Reboot]
```

Then, to stop the system's copy of `gde-background` and prevent
`systemd` from restarting it:

```bash
$ systemctl --user stop gde-background
```

Finally, hit run in GNOME Builder
