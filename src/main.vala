using Gdk;
namespace Background {
    void main(string[] args) {
        Gtk.init(ref args);

        var disp = Display.get_default();
        for (int i = 0; i < disp.get_n_monitors(); i++)
            create_window(disp.get_monitor(i));
        disp.monitor_added.connect(monitor => create_window(monitor));
        disp.monitor_removed.connect(monitor => destroy_window(monitor));

        Gtk.main();
    }

    void create_window(Monitor monitor) {
        var win = new Window();
        GtkLayerShell.set_monitor(win, monitor);
        monitor.set_data<Window>("gde-background", win);
        win.show_all();
    }

    void destroy_window(Monitor monitor) {
        monitor.get_data<Window>("gde-background").destroy();
    }
}
