using Gdk;
using GDesktop;
namespace Background {
    void pixbuf_blend_fit(Pixbuf dest, Pixbuf src, int alpha) {
        var ratio_w = (double) dest.width / src.width,
            ratio_h = (double) dest.height / src.height;
        var ratio = ratio_w < ratio_h ? ratio_w : ratio_h;

        var final_width = (int)(ratio * src.width),
            final_height = (int)(ratio * src.height);
        var off_x = (dest.width - final_width) / 2,
            off_y = (dest.height - final_height) / 2;

        src.composite(dest, off_x, off_y, final_width, final_height, off_x,
            off_y, ratio, ratio, Gdk.InterpType.BILINEAR, alpha);
    }

    void pixbuf_blend_scale(Pixbuf dest, Pixbuf src, int alpha) {
        var scl_w = (double) dest.width / src.width,
            scl_h = (double) dest.height / src.height;
        var factor = scl_w > scl_h ? scl_w : scl_h;

        var final_width = (int)(factor * src.width),
            final_height = (int)(factor * src.height);
        var off_x = (dest.width - final_width) / 2,
            off_y = (dest.height - final_height) / 2;

        src.composite(dest, 0, 0, dest.width, dest.height, off_x, off_y, factor,
            factor, Gdk.InterpType.BILINEAR, alpha);
    }

    void pixbuf_blend(Pixbuf dest, Pixbuf src, int alpha, int mode) {
        if (mode == BackgroundStyle.NONE)
            return;
        else if (mode == BackgroundStyle.SCALED)
            pixbuf_blend_fit(dest, src, alpha);
        //TODO: Tile image mode
        //else if (mode == BackgroundStyle.SPANNED)
        //    pixbuf_blend_tile(dest, src, alpha);
        else
            pixbuf_blend_scale(dest, src, alpha);
    }
}
