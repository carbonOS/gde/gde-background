using Gnome;
using GDesktop;

class Background.TimedWallpaper {
    Window window;
    BGSlideShow slideshow;
    int bg_mode;
    int max_alpha;
    uint? sched = null;

    public TimedWallpaper(Window window, string path, int mode, int max_alpha) {
        this.window = window;
        this.bg_mode = mode;
        this.max_alpha = max_alpha;

        slideshow = new BGSlideShow(path);
        try {
            slideshow.load();
        } catch (Error e) {
            warning("Failed to load timed wallpaper: %s", e.message);
            return;
        }

        render_slide(); // Render the first slide
    }

    private bool render_slide() {
        var pixbuf = window.pixbuf;
        var width = window.pixbuf.width, height = window.pixbuf.height;
        double progress, duration;
        bool fixed;
        string file1, file2;
        slideshow.get_current_slide(width, height, out progress, out duration,
            out fixed, out file1, out file2);

        if (fixed)
            info("Slide(static): %s", file1);
        else
            info("Slide(transition @ %f): %s -> %s", progress, file1, file2);

        var img1 = new Gdk.Pixbuf.from_file(file1);
        pixbuf_blend(pixbuf, img1, max_alpha, bg_mode);

        if (!fixed) {
            var img2 = new Gdk.Pixbuf.from_file(file2);
            pixbuf_blend(pixbuf, img2, (int)(max_alpha * progress), bg_mode);
        }

        // Schedule the next slide
        // For fixed frames, updates are scheduled for when the frame expires
        // For transitions, updates are scheduled 4 times for each alpha value
        var delay = fixed ? ((1.0 - progress) * duration) : (duration / 1020);
        if (delay < 0.16) delay = 0.16;
        delay *= 1000;
        sched = Timeout.add((uint) delay, render_slide);

        window.queue_draw();
        return false;
    }

    public void destroy() {
        if (sched != null) Source.remove(sched);
    }
}
