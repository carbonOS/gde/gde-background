using GDesktop;
class Background.Window : Gtk.Window {
    Settings settings;
    Settings style_mgr;
    FileMonitor file_monitor;
    public Gdk.Pixbuf pixbuf;
    TimedWallpaper timed = null;

    public Window() {
        decorated = false;
        title = "__gde_background";
        draw.connect(cr => {
            cr.save();
            cr.scale(1.0 / this.scale_factor, 1.0 / this.scale_factor);
            Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0);
            cr.paint();
            cr.restore();
            return true;
        });
        size_allocate.connect(alloc => reset());

        GtkLayerShell.init_for_window(this);
        GtkLayerShell.set_exclusive_zone(this, -1); // Ignore panel exclusive zone
        GtkLayerShell.set_layer(this, GtkLayerShell.Layer.BACKGROUND);
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.LEFT, true);
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.RIGHT, true);
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.TOP, true);
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.BOTTOM, true);

        settings = new Settings("org.gnome.desktop.background");
        settings.changed.connect(opt => reset());
        style_mgr = new Settings("org.gnome.desktop.interface");
        style_mgr.changed["color-scheme"].connect(() => reset());
    }

    public void reset(int width = -1, int height = -1) {
        if (width == -1 && height == -1) {
            this.get_size(out width, out height);
            width *= this.scale_factor;
            height *= this.scale_factor;
        }

        // Stop timed wallpaper if it exists
        if (timed != null) {
            timed.destroy();
            timed = null;
        }

        // Setup the base pixbuf (solid color)
        pixbuf = new Gdk.Pixbuf(Gdk.Colorspace.RGB, false, 8, width, height);
        var fill_color = Gdk.RGBA();
        if (fill_color.parse(settings.get_string("primary-color")))
            pixbuf.fill(fill_color.hash());
        else
            pixbuf.fill(0x000000FF); // Black

        // Check the background mode & picture-alpha
        var bg_mode = settings.get_enum("picture-options");
        var max_alpha = 255.0 * (settings.get_int("picture-opacity") / 100.0);
        if (bg_mode == BackgroundStyle.NONE || max_alpha == 0) {
            this.queue_draw();
            return;
        }

        // Get the background's path
        var dark = style_mgr.get_enum("color-scheme") == ColorScheme.PREFER_DARK;
        var path = settings.get_string(dark ? "picture-uri-dark" : "picture-uri");
        var scheme = Uri.parse_scheme(path);
        if (scheme != null) {
            if (scheme == "file") {
                try {
                    path = Filename.from_uri(path);
                } catch (Error e) {
                    critical(e.message);
                    this.queue_draw();
                    return;
                }
            } else {
                warning("%s:// URIs are not supported. Try file://", scheme);
                this.queue_draw();
                return;
            }
        }

        // Reset File Watch
        if (file_monitor != null) {
            file_monitor.cancel();
            file_monitor = null;
        }
        var bg_file = File.new_for_path(path);
        try {
            file_monitor = bg_file.monitor(FileMonitorFlags.NONE);
            file_monitor.changed.connect(() => reset());
        } catch (Error e) {
            critical("Failed to monitor file: %s", e.message);
        }

        // Apply the wallpaper
        if (path.has_suffix(".xml")) { // GNOME timed wallpaper
            info("Setting timed wallpaper: %s", path);
            timed = new TimedWallpaper(this, path, bg_mode, (int) max_alpha);
        } else { // Regular Image
            info("Setting regular wallpaper: %s", path);
            try {
                var img = new Gdk.Pixbuf.from_file(path);
                pixbuf_blend(pixbuf, img, (int) max_alpha, bg_mode);
            } catch (Error e) {
                warning("Failed to load image: %s", e.message);
            }
        }

        this.queue_draw();
    }
}
